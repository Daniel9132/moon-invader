﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    public float speed = 10f;

   
   
    private Transform target;
    private int wavepointIndex = 0;

    public int starthealth = 100;
    private float health;

    public int value = 50;
    public GameObject deathEffect;

    [Header("Unity Stuff")]
    public Image healthBar;


   

    public void TakeDamage (int amount)
    {
        health -= amount;

        healthBar.fillAmount = health/starthealth;

        if (health <= 0)
        {
            Debug.Log("mas monedas");
            PlayerStats.Money = PlayerStats.Money + value;
            Die();
            
        }
    }

    void Die ()
    {
        GameObject effect = (GameObject)Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(effect, 5f);
        PlayerStats.Money += value;

        Spawner.EnemiesAlive--;

        Destroy(gameObject);
    }
  
    void Start ()
    {
        health = starthealth;
        target = Waypoints.points[0];
        
    }

    void Update ()
    {

        transform.Rotate(new Vector3(0f, 50f, 0f)*Time.deltaTime);

        Vector3 dir = target.position - transform.position;
        transform.Translate(dir.normalized*speed*Time.deltaTime , Space.World);
        

        if (Vector3.Distance(transform.position,target.position)<= 0.4f)
        {
            
            GetNextWaypoint();
            
            
        }
    }

    void GetNextWaypoint()
    {
        if(wavepointIndex >= Waypoints.points.Length -1)
        {
            EndPath();
            return;
        }

        wavepointIndex++;

        
        target = Waypoints.points[wavepointIndex];


    }

    void EndPath()
    {
        PlayerStats.Lives --;
        Spawner.EnemiesAlive--;
        Destroy(gameObject);
    }
}
