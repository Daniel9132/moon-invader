﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy_Controller : MonoBehaviour
{
    void Start()
    {
        Enemy_Model EnemyModel = GetComponent<Enemy_Model>();
        EnemyModel.health = EnemyModel.starthealth;
        EnemyModel.target = Waypoints.points[0];

    }

    void Update()
    {
        Enemy_View EnemyView = GetComponent<Enemy_View>();
        Enemy_Model EnemyModel = GetComponent<Enemy_Model>();
        transform.Rotate(new Vector3(0f, 50f, 0f) * Time.deltaTime);

        Vector3 dir = EnemyModel.target.position - transform.position;
        transform.Translate(dir.normalized * EnemyModel.speed * Time.deltaTime, Space.World);


        if (Vector3.Distance(transform.position, EnemyModel.target.position) <= 0.4f)
        {

            EnemyView.GetNextWaypoint();


        }
    }
}
