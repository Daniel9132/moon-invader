﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy_Model : MonoBehaviour
{
    public float speed = 10f;



    public Transform target;
    public int wavepointIndex = 0;

    public int starthealth = 100;
    public float health;

    public int value = 50;
    public GameObject deathEffect;

    [Header("Unity Stuff")]
    public Image healthBar;
}
