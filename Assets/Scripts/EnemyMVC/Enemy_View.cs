﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy_View : MonoBehaviour
{

    public void TakeDamage(int amount)
    {
        
        Enemy_Model EnemyModel = GetComponent<Enemy_Model>();

        EnemyModel.health -= amount;

        EnemyModel.healthBar.fillAmount = EnemyModel.health / EnemyModel.starthealth;

        if (EnemyModel.health <= 0)
        {
            Debug.Log("mas monedas");
            PlayerStats.Money = PlayerStats.Money + EnemyModel.value;
            Die();

        }
    }
    public void Die()
    {
        Enemy_Model EnemyModel = GetComponent<Enemy_Model>();

        GameObject effect = (GameObject)Instantiate(EnemyModel.deathEffect, transform.position, Quaternion.identity);
        Destroy(effect, 5f);
        PlayerStats.Money += EnemyModel.value;

        Spawner.EnemiesAlive--;

        Destroy(gameObject);
    }

    public void GetNextWaypoint()
    {
        Enemy_Model EnemyModel = GetComponent<Enemy_Model>();

        if (EnemyModel.wavepointIndex >= Waypoints.points.Length - 1)
        {
            EndPath();
            return;
        }

        EnemyModel.wavepointIndex++;


        EnemyModel.target = Waypoints.points[EnemyModel.wavepointIndex];


    }

    public void EndPath()
    {
        PlayerStats.Lives--;
        Spawner.EnemiesAlive--;
        Destroy(gameObject);
    }
}
