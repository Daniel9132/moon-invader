﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUN : MonoBehaviour
{
    public float range = 100f;
    public float damage = 10f;
    public GameObject effect;
   
    // Start is called before the first frame update
   
    void Update()
    {

        if (Input.GetMouseButtonDown(1))
        {
            RaycastHit hit;

            if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward , out hit, range))
            {
                Debug.Log(hit.collider.name);
                Instantiate(effect, hit.point, Quaternion.identity);
                
            }
        }
    }

    private void OnDrayGuizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(Camera.main.transform.position, Camera.main.transform.forward * range);
    }

}
