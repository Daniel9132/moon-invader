﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Build_Manager : MonoBehaviour
{
    public static Build_Manager instance;
    
    void Awake()
    {
        
        if (instance != null)
        {
            Debug.LogError("More than one BuildManager in scene");
            return;
        }
        instance = this;
    }
    public GameObject standartTurretPrefab1;
    public GameObject standartTurretPrefab2;
    public GameObject standartTurretPrefab3;

    void Start()
    {
        turretToBuild1 = standartTurretPrefab1;
        turretToBuild2 = standartTurretPrefab2;
        turretToBuild3 = standartTurretPrefab3;
    }

    public GameObject turretToBuild1;
    public GameObject turretToBuild2;
    public GameObject turretToBuild3;
    public GameObject GetTurretToBuild1 ()
    {
        return turretToBuild1;
        
    }
    public GameObject GetTurretToBuild2()
    {
        return turretToBuild2;

    }
    public GameObject GetTurretToBuild3()
    {
        return turretToBuild3;

    }

}
