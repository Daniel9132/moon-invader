﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{

    
    private GameObject turret1;
    private GameObject turret2;
    private GameObject turret3;

    public Vector3 positionOffset;

    public  int type;
    public int turrettype=1;

    void Start ()
    {
        type = turrettype;
    }

    void Update()
    {

        if (Input.GetMouseButtonDown(1) && type < 4)
        {
            type++;
        }
        else
        {
            if(type >= 4)
            {
                type = 1;
                Debug.Log("Reinicio de torretas");
            }
           
        }
        

    }
        void OnMouseDown()
    {
        if (turret1 || turret2 || turret3 != null)
        {
            Debug.Log("No puedes construir aqui");
            return;
        }
        else
        { if (type == 1 && PlayerStats.Money >= 100)
            {
                PlayerStats.Money = PlayerStats.Money - 100;
                GameObject turretToBuild = Build_Manager.instance.GetTurretToBuild1();
                turret1 = (GameObject)Instantiate(turretToBuild, transform.position + positionOffset, transform.rotation);
            }
            
            if (type == 2 && PlayerStats.Money >= 250)
            {
                PlayerStats.Money = PlayerStats.Money - 250;
                GameObject turretToBuild = Build_Manager.instance.GetTurretToBuild2();
                turret2 = (GameObject)Instantiate(turretToBuild, transform.position + positionOffset, transform.rotation);
            }
            if (type == 3 && PlayerStats.Money >= 400)
            {
                PlayerStats.Money = PlayerStats.Money - 400;
                GameObject turretToBuild = Build_Manager.instance.GetTurretToBuild3();
                turret3 = (GameObject)Instantiate(turretToBuild, transform.position + positionOffset, transform.rotation);
            }
            else
            {
                Debug.Log("Insuficiente energia");
            }
        }
        
    }
  
        

    
   
}
