﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLook_Controller : MonoBehaviour
{
    private void LockCursor()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void ClampXAxisRotationToValue(float value)
    {
        Vector3 eulerRotation = transform.eulerAngles;
        eulerRotation.x = value;
        transform.eulerAngles = eulerRotation;
    }
}
