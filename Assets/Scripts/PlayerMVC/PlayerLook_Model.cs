﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLook_Model : MonoBehaviour
{
    [SerializeField]
    public string mouseXInputName, mouseYInputName; // camara arriba y abajo
    [SerializeField]
    public float mouseSensitivity; // Sensibilidad 
    [SerializeField]
    public Transform playerBody; // camara izquierda y derecha

    public float xAxisClamp;
}
