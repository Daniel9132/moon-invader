﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLook_View : MonoBehaviour
{
    public void Update()
    {
        CameraRotation();
    }

    public void CameraRotation()
    {
        PlayerLook_Model PlayerModel = GetComponent<PlayerLook_Model>();
        PlayerLook_Controller PlayerController = GetComponent<PlayerLook_Controller>();

        float mouseX = Input.GetAxis(PlayerModel.mouseXInputName) * PlayerModel.mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis(PlayerModel.mouseYInputName) * PlayerModel.mouseSensitivity * Time.deltaTime;

        PlayerModel.xAxisClamp += mouseY;

        if (PlayerModel.xAxisClamp > 90.0f)        //Para que la camara no se de toda la vuelta y se detenga cuando miras arriba o abajo
        {
            PlayerModel.xAxisClamp = 90.0f;
            mouseY = 0.0f;

            PlayerController.ClampXAxisRotationToValue(270.0f);
        }
        else if (PlayerModel.xAxisClamp < -90.0f)
        {
            PlayerModel.xAxisClamp = -90.0f;
            mouseY = 0.0f;

            PlayerController.ClampXAxisRotationToValue(90.0f);
        }


        transform.Rotate(Vector3.left * mouseY);//De este codigo hasta donde empieza la funcion es el movimiento de arriba y abajo
        PlayerModel.playerBody.Rotate(Vector3.up * mouseX); // con esto se mueve a los izquierda y derecha
    }
}
