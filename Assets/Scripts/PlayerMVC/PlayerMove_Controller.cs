﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove_Controller : MonoBehaviour
{
    public void Awake()
    {
        PlayerMove_Model PlayerMove = GetComponent<PlayerMove_Model>();
        PlayerMove.CharController = GetComponent<CharacterController>();
    }

    public void Update()
    {
        PlayerMove_View PlayerView = GetComponent<PlayerMove_View>();
        PlayerView.PlayerMovement();
    }

    public IEnumerator JumpEvent()
    {
        PlayerMove_Model PlayerMove = GetComponent<PlayerMove_Model>();

        PlayerMove.CharController.slopeLimit = 90.0f; // para que pueda saltar aunque este pegado a una pared
        float timeInAir = 0.0f;
        do
        {
            float jumpforce = PlayerMove.jumpFallOff.Evaluate(timeInAir);
            PlayerMove.CharController.Move(Vector3.up * jumpforce * PlayerMove.JumpMultiplier * Time.deltaTime);
            timeInAir += Time.deltaTime;

            yield return null;
        } while (!PlayerMove.CharController.isGrounded && PlayerMove.CharController.collisionFlags != CollisionFlags.Above);
        PlayerMove.CharController.slopeLimit = 45.0f; // para que pueda saltar aunque este pegado a una pared
        PlayerMove.isJumping = false;
    }
}
