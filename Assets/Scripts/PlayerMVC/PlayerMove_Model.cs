﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove_Model : MonoBehaviour
{
    [SerializeField]
    public string horizontalInputName;
    [SerializeField]
    public string verticalInputName;
    [SerializeField]
    public float movementspeed; // que sea 6 pendejo

    public CharacterController CharController;
    public bool isJumping;

    [SerializeField]
    public AnimationCurve jumpFallOff; // velocidad de caida
    [SerializeField]
    public float JumpMultiplier;
    [SerializeField]
    public KeyCode jumpkey;

    void start()
    {
        Cursor.visible = false;
    }
 
}
