﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove_View : MonoBehaviour
{
    public void PlayerMovement()
    {
        PlayerMove_Model PlayerMove = GetComponent<PlayerMove_Model>();
        float horizInput = Input.GetAxis(PlayerMove.horizontalInputName) * PlayerMove.movementspeed;
        float vertInput = Input.GetAxis(PlayerMove.verticalInputName) * PlayerMove.movementspeed;

        Vector3 forwarmovement = transform.forward * vertInput;
        Vector3 rightmovement = transform.right * horizInput;

        PlayerMove.CharController.SimpleMove(forwarmovement + rightmovement);

        JumpInput();
    }

    public void JumpInput()
    {
        PlayerMove_Model PlayerMove = GetComponent<PlayerMove_Model>();
        PlayerMove_Controller PlayerController = GetComponent<PlayerMove_Controller>();

        if (Input.GetKeyDown(PlayerMove.jumpkey) && !PlayerMove.isJumping)
        {
            PlayerMove.isJumping = true;
            StartCoroutine(PlayerController.JumpEvent());
        }
    }
}
