﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLook : MonoBehaviour
{
    [SerializeField]
    private string mouseXInputName, mouseYInputName; // camara arriba y abajo
    [SerializeField]
    private float mouseSensitivity; // Sensibilidad 
    [SerializeField]
    Transform playerBody; // camara izquierda y derecha

    private float xAxisClamp;

    private void Awake()
    {
        LockCursor();
        xAxisClamp = 0.0f;
    }

    private void LockCursor()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        CameraRotation();
    }

    private void CameraRotation()
    {
        float mouseX = Input.GetAxis(mouseXInputName) * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis(mouseYInputName) * mouseSensitivity * Time.deltaTime;

        xAxisClamp += mouseY;

        if(xAxisClamp >90.0f)        //Para que la camara no se de toda la vuelta y se detenga cuando miras arriba o abajo
        {
            xAxisClamp = 90.0f;
            mouseY = 0.0f;
            ClampXAxisRotationToValue(270.0f);
        }
        else if (xAxisClamp < -90.0f)
        {
            xAxisClamp = -90.0f;
            mouseY = 0.0f;

            ClampXAxisRotationToValue(90.0f);
        }


        transform.Rotate(Vector3.left * mouseY);//De este codigo hasta donde empieza la funcion es el movimiento de arriba y abajo
        playerBody.Rotate(Vector3.up * mouseX); // con esto se mueve a los izquierda y derecha
    }

    private void ClampXAxisRotationToValue(float value)
    {
        Vector3 eulerRotation = transform.eulerAngles;
        eulerRotation.x = value;
        transform.eulerAngles = eulerRotation;
    }
}
