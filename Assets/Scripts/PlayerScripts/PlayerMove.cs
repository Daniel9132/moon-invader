﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    [SerializeField]
    private string horizontalInputName;
    [SerializeField]
    private string verticalInputName;
    [SerializeField]
    private float movementspeed; // que sea 6 pendejo

    private CharacterController CharController;
    private bool isJumping;

    [SerializeField]
    private AnimationCurve jumpFallOff; // velocidad de caida
    [SerializeField]
    private float JumpMultiplier;
    [SerializeField]
    private KeyCode jumpkey;

    private void Awake()
    {
        CharController = GetComponent<CharacterController>();
    }

    private void Update()
    {
      
        PlayerMovement();
    }

    private void PlayerMovement()
    {
        float horizInput = Input.GetAxis(horizontalInputName) * movementspeed;
        float vertInput = Input.GetAxis(verticalInputName) * movementspeed;

        Vector3 forwarmovement = transform.forward * vertInput;
        Vector3 rightmovement = transform.right * horizInput;

        CharController.SimpleMove(forwarmovement + rightmovement);

        JumpInput();
    }

    private void JumpInput()
    {
        if(Input.GetKeyDown(jumpkey) && !isJumping)
        {
            isJumping = true;
            StartCoroutine(JumpEvent());
        }
    }

    private IEnumerator JumpEvent()
    {
        CharController.slopeLimit = 90.0f; // para que pueda saltar aunque este pegado a una pared
        float timeInAir = 0.0f;
        do
        {
            float jumpforce = jumpFallOff.Evaluate(timeInAir);
            CharController.Move(Vector3.up * jumpforce * JumpMultiplier * Time.deltaTime);
            timeInAir += Time.deltaTime;

            yield return null;
        } while (!CharController.isGrounded && CharController.collisionFlags != CollisionFlags.Above );

        CharController.slopeLimit = 45.0f; // para que pueda saltar aunque este pegado a una pared
        isJumping = false;
    }

}
