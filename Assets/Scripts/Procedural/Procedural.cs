﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Procedural : MonoBehaviour
{
    public Transform spawnpoint;
    public Transform level;
    

    void Start()
    {
        Time.timeScale = 1f;
        Instantiate(level, spawnpoint.position, spawnpoint.rotation);
        Cursor.visible = false;

    }

 
}
