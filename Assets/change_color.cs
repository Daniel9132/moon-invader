﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class change_color : MonoBehaviour
{

    public Color seleccionado;
    public Color colorInicial;
    private Renderer render;
   

    void Start()
    {
        render = GetComponent<Renderer>();
        colorInicial = render.material.color;
    }
    public void OnMouseOver()
    {
        render.material.color = seleccionado;
    }

    public void OnMouseExit()
    {
        render.material.color = colorInicial;
    }

}
